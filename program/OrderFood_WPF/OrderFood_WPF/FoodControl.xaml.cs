﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderFood_WPF
{
    /// <summary>
    /// FoodControl.xaml 的交互逻辑
    /// </summary>
    public partial class FoodControl : UserControl
    {//菜品控件；
        public Food food;
        public FoodControl()
        {
            InitializeComponent();
        }
        public FoodControl(Food food)
        {
            InitializeComponent();
            this.food = food;
            lab_foodName.Content = food.food_name;
            lab_foodPrice.Content = "￥"+food.food_price;

            //载入图片；
            Uri uri = new Uri(System.Environment.CurrentDirectory + "\\" + food.food_pic_url, UriKind.Absolute);
            BitmapImage bitmap = new BitmapImage(uri);
            image.Source = bitmap;           
        }
        private void cb_check()
        {
            if (cb.IsChecked != true)
                cb.IsChecked = true;
            else
                cb.IsChecked = false;
        }
        private void FC_UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            cb_check();
        }
        private WrapPanel get_wp_ordered()
        {//获得主窗口的相关控件；
            var wrapPanal_food = (this.Parent as WrapPanel);
            var scrollViewer2 = (wrapPanal_food.Parent as ScrollViewer);
            var mainGrid = (scrollViewer2.Parent as Grid);
            var scrollViewer = (mainGrid.Parent as ScrollViewer);
            var mainWindow = (scrollViewer.Parent as MainWindow);
            return mainWindow.wp_ordered;
        }
        private void cb_Checked(object sender, RoutedEventArgs e)
        {
            UC_FoodBtn foodBtn = new UC_FoodBtn(this);
            WrapPanel wp_ordered = get_wp_ordered();
            wp_ordered.Children.Add(foodBtn);
        }
        private void cb_Unchecked(object sender, RoutedEventArgs e)
        {//菜单取消勾选后移除已选列表中的相关项；
            WrapPanel wp_ordered = get_wp_ordered();
            UC_FoodBtn theBtn = null;
            foreach (UIElement element in wp_ordered.Children)
            {
                theBtn = (UC_FoodBtn)element;
                string str1 = theBtn.fc.food.food_name;
                string str2 = this.food.food_name;
                if(str1.Equals(str2))
                {
                    break;
                }
            }
            wp_ordered.Children.Remove(theBtn);
        }
    }
}
