﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderFoodAdmin
{
    /// <summary>
    /// UC_FoodItem.xaml 的交互逻辑
    /// </summary>
    public partial class UC_FoodItem : UserControl
    {
        public UC_FoodItem()
        {
            InitializeComponent();
        }
        public UC_FoodItem(string lab_foodName, float lab_foodPrice)
        {
            InitializeComponent();
            this.lab_foodName.Content = lab_foodName;
            this.lab_foodPrice.Content = lab_foodPrice;
            btn_deleteFood.IsEnabled = false;
        }

        private void btn_deleteFood_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("此操作将删除数据库中相关项目，您确定要执行吗？", "警告！", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    //取得主窗口；
                    var stackPanel = (this.Parent as StackPanel);
                    var scrollViewer = (stackPanel.Parent as ScrollViewer);
                    var grid1 = (scrollViewer.Parent as Grid);
                    var tabItem = (grid1.Parent as TabItem);
                    var tabControl = (tabItem.Parent as TabControl);
                    var grid2 = (tabControl.Parent as Grid);                
                    var mainWindow = (grid2.Parent as MainWindow);

                    mainWindow.deleteFood(this.lab_id.Content.ToString());
                }
                catch (NullReferenceException ex)
                {
                    MessageBox.Show("未找到相关父控件！"+ex);
                }
                
            }
        }
    }
}
