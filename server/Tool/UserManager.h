#ifndef ZQH_QQ_USERMANAGER_H__
#define ZQH_QQ_USERMANAGER_H__

#include <winsock.h>

typedef struct PwdItem
{
	char szName[20];
	char szPassword[20];
}PwdItem;

typedef struct AuthUser
{
	char szName[20];
	SOCKET sock;
}AuthUser;

extern PwdItem g_PwdItemList[1024];
extern int g_nPwdItemNum;

extern AuthUser g_UserList[1024];
extern int  g_nUserNum;

bool LoadPwdItems(const char* pszFileName);
bool FindUser(const char* pszName, const char* pszPassword);

void AddUser(const char* pszName, SOCKET s);
void DeleteUser(const char* pszName);
bool HasAuth(SOCKET s);

#endif //ZQH_QQ_USERMANAGER_H__